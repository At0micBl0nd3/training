# Task 7

#### Reading

Read chapters 13 and 14 of Skylaroff.  

Read chapter 2 of **Hacking: The Art of Exploitation**.  

#### Exercises

Work through the OverTheWire wargames Leviathan, Narnia, and Behemoth.  

Write up your solutions in markdown.  