# Task 2

#### Reading

Read chapters 3 and 4 of **The C Programming Language**.  

Tips:

- It is helpful to put main() at the end of a file.
- Ideally the main function is on it's own in main.c, and all other functions are in another file.
- Try some of the example programs in the book.
- Henceforth, **The C Programming Language** is referred to as "K&R" for the authors.

Also read chapter 1, sections 1 and 2 of **Cryptography: An Introduction**.  
So stop when you get to the "3. Basic Algorithms" heading.  

Tips:

- Look up what the logical symbols mean [here](https://en.wikipedia.org/wiki/List_of_logic_symbols).
- Proofs take time, think through before googling.  If you do google, ensure that you understand.

#### Questions

1. Groups of statements enclosed in braces are called what?
2. What is a while loop that is equivalent to "for(int i = 3; i <= 2019; i+=19)"?
3. What is the difference between a while and do-while loop?
4. What is the general syntax of a function declaration?
5. What does "#include" do?
6. What is a static variable, and what is a register variable?
7. What is the difference between declaration and initialization?
8. What is the definition of congruence modulo n?
9. What is the definition of a group?
10. What is the name for a group with a commutative operation?
11. What is an example of a commutative group?
12. What does Euler's Totient Function do?
13. Do integers always have multiplicative inverses modulo n (for any n)?
14. What is 5^19 in the group of the first 19 integers (0 through 18) under multiplication?
15. What is the definition of the characteristic of a field?

#### Exercises

1. K&R Exercise 3-3.
2. K&R Exercise 3-5.
3. K&R Exercise 4-2.
4. K&R Exercise 4-3.
5. K&R Exercise 4-4.
6. K&R Exercise 4-6.
7. K&R Exercise 4-13.
8. Write a program (in C) that prints a "multiplication table" for the group of 0 through (n - 1) under multiplication, for inputted n. 
9. Prove the basic lemma: If a = b (mod n) and c = d (mod n); then (a + c) = (b + d) (mod n).  "=" means congruent here.
10. Prove that for any integer n, n^2 (mod 4) is either 0 or 1.  
11. Prove that a^b (mod n) = a^(b (mod phi(n))) (mod n), where phi is Euler's totient function, and n is prime.  
