# Task 6

#### Reading

Read chapter 1 of **Practical Reverse Engineering** by Dang.  

#### Questions

None.

#### Exercises

All exercises in chapter 1.  