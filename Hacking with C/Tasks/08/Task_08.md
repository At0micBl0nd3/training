# Task 8

#### Reading

Read Smart chapters 6, 7, 8.  

Read the SSH and TLS protocols in the **Networking Protocols Handbook**.

#### Exercises

1. Cryptopals set 1
2. Cryptopals set 2
3. Cryptopals set 3
4. Cryptopals set 4
