# Task 3

This task is somewhat harder and more advanced than the last ones.  Let 
me know if you have questions or are struggling.  Don't be afraid to 
search the web for questions either.  You are going to make mistakes, so 
record what the mistake was and keep on the lookout for it as you 
continue to program more.  There are a couple more tasks of heavy C learning 
after this one, then we move into reversing and exploitation.  

#### Reading

Read chapters 5 and 6 in K&R.  These are key concepts that 
start to put everything together and make C very powerful
for you to use.  Pointers are the most important (and most 
messed up) thing in C.  

Tips:

- Always use calloc, not malloc.  
- Keep pointer type in mind when doing arithmetic.
- Always use the \* symbol, stop using the "[]" symbols to define arrays.
- argv is powerful, but be careful with it.
- If your program crashes, it is probably a pointer issue.

Review the first two sections of chapter 1 in Smart (**Cryptography: An Introduction**).  
Pay careful attention to definitions.  Then read chapters 1, 2, and 4 in **Book of Proof** by 
Hammack.  

Read pages 1-10 (the first chapter) of the **Network Protocols Handbook**.  
The goal is a high level understanding of the concepts at this point.

#### Questions

1. Define a pointer.
2. What is the unary "\*" operator in C?
3. What is the unary "&" operator in C?
4. How does pointer arithmetic work?
5. Read the man page for "calloc" and summarize what it does.  You can find by typing "man calloc" in your terminal.
6. What does a function pointer do?
7. What is a struct?
8. What are the two ways of accesing the members of a struct?
9. How does a tree work?
10. How does a linked list work?
11. What is a union?
12. What do each of the 7 OSI layers do?  Give examples.
13. What is a network protocol?
14. What is the definition of a ring?
15. Do groups have an identity element?

#### Exercises

1. Find 5 modular arithmetic proof exercises online.  State and prove them (and send me the source).
2. Find 5 basic set theory proof exercises online.  State and prove them (and send me the source).
3. K&R Exercise 5-2.
4. K&R Exercise 5-3.
5. K&R Exercise 5-5.  Think about how this might affect security.
6. K&R Exercise 5-13.  Then type "man tail" in your terminal and read it.
7. K&R Exercise 6-1.
8. K&R Exercise 6-4.
9. Write a program to implement Base64 coding and decoding.
10. Write a program to implement a linked list.  Include functionality to create, add, remove, delete, and sort lists.  



