# Task 11

This one might take awhile.  

#### Reading

Read all of Skylaroff.

#### Setup/Research

Find an old version of Ubuntu and make a VM called "Target_UB".  Also 
make a Windows 7 VM called "Target_W7".  Research the versions you 
chose.  On the Ubuntu, install an old version of any server software 
(ssh, http server, etc).  On the Windows 7, do the same.  Research the 
software installed.  Find exploits for the versions of software on 
the machines.  Also, find local privilege escalation exploits for 
each of the machines.  

Make a virtual network with both of these machines, and one of your 
CyberLearn VMs.  Assign the name "c2.delogrand.us" to your attack VM.  

#### Programming

Program backdoor programs for both Windows and Linux.  

Program software that runs the local privilege escalation exploits 
on the targets.  This will be 2 pieces of software (one for Win, one 
for Linux).  

Program software that scans an ip range for vulnerable versions of the server 
software for the exploits you chose (remote exploits - not the priv 
escalation ones).  Upon finding one, launch the exploit.  

Program software that will find the password files and exfiltrate 
them, along with any file in user directories that contains "interesting" 
information.  

Program software to remove any of your programs on the system, and 
then itself (aka hiding tracks).  

#### Execution

Put this all together and test by taking over both target systems.  