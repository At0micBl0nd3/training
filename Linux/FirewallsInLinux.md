# Host Based Firewalls in Linux
by firef0x

------



## Install and set up
Iptables should be installed already, but if not, it can installed with `sudo apt-get install iptables` or `sudo apt install iptables`. The version can be checked with the `-V` option: `iptables -V`. (Lowercase `-v` is the verbose option)



## Showing the rules
List the current rules with `iptables -L -v`. Verbose should give more information. With no rules, it should look like this:
```bash
root@kali:~# iptables -L -v
Chain INPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         

Chain OUTPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination    
```
There are 3 different chains here, input, forward, output. Input controls what happens to incoming packets. Forward deals with incoming packets that will be forwarded somewhere else. Output handles outgoing packets.



## Making rules
Rules are added by appending them to a chain with `-A` or inserting them with `-I`. When you add rules to iptables, it either appends, `-A`, them to the end of the chain (the ordered list of rules) or inserts, `-I`, them in front of the first rule.
```bash
iptables -A [chain] [rest of rule] # adds rule to the end of chain
```
Next, you add on to see what/how you want to filter
- **-i:  interface** This option lets you filter on which network interface the traffic is coming in on, like eth0, lo, etc
- **-p:  protocol** Selects by protocol, tcp, udp, icmp, icmpv6, smtp, etc
- **-s:  source** traffic's source IP or hostname.
- **--dport:  destination port** (INPUT) filters on the destination port. Under normal circumstances, you can use this to limit types of traffic like ssh or ftp, but these services can be run on their nonstandard ports. If you specify a port, you must specify a protocol because a port could be on TCP or UDP.
- **--sport:  source port** (OUTPUT) filters for the port where the traffic departs from
- **-o:  output interface** the output interface
- **-j:  target** This is either ACCEPT, DROP, or RETURN and needs to be present when you make a new rule.

The basic command takes this format:
```bash
sudo iptables -A <chain> -i <interface> -p <protocol (tcp/udp) > -s <source> --dport <port no.>  -j <target>
```
To drop all udp traffic for example, run
```bash
iptables -A INPUT -p udp -j DROP
```
This only blocks inbound UDP traffic. outbound traffic is still allowed to leave. To delete this rule to allow udp traffic back in, use -D instead of -A
```bash
iptables -D INPUT -p udp -j DROP
```

### Some Example Rules
#### Filtering by Service
Allow ssh, http, https, pop3 (110, 995 for encrypted) by allowing their standard ports. Switch ACCEPT to DROP to deny any of these. The -`A` signified adding a new rule. `-D` will delete a rule.
```bash
# must execute with sudo or as root
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp --dport 110 -j ACCEPT
iptables -A INPUT -p tcp --dport 995 -j ACCEPT
iptables -A INPUT -j DROP # disallow all traffic not allowed
```
The resulting rules are added in the order they were entered, and the dport is written as a service here.
```bash
root@ubuntu:~# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ssh
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:http
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:https
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:pop3
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:pop3s
DROP       all  --  anywhere             anywhere 
```



#### Blocking or Allowing certain Source IPs

Use `-s` to block or allow a specific IP.
```bash
iptables -A INPUT -s 36.2.155.44 -j ACCEPT
```
To block an IP range, use the following syntax
```bash
iptables -A INPUT -m iprange --src-range 100.200.150.1-100.200.150.7 -j ACCEPT
```



#### Dropping All Other traffic

One way to use iptables is as a whitelist, enumerating allowed traffic then blocking all other traffic. To do this, add all your accept rules then have the final rule drop all other traffic. Be careful not to have rules after this because they will not be evaluated.
```bash
iptables -A INPUT -j DROP
```



#### Deleting Rules Quickly

One quick way to delete rules is based off there number. To show the numbers of the rules you currently have, run `iptables -L --line-numbers`. This will add a number column at the left where you can delete rules by number with  `iptables -D INPUT 6` where 6 is the number of the rule you wish to delete. Be careful when deleting more than one rule at once because the indices change after deleting the first rule. For example, if you delete rule 3, rule 4 because the new rule 3.



#### Connection based Filtering
The `--ctstate` option allows you to filter based on connection type. The option are `NEW`, `RELATED`, `ESTABLISHED`, `INVALID`. The following rule allows all established connections.
```bash
iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
```
The -m option allows additional options for connection tracking here.



## Example rules

```bash
iptables -A INPUT -i lo -j ACCEPT   # accept all loopback traffic
iptables -A OUTPUT -o lo -j ACCEPT
iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT  # allow existing connections
iptables -A INPUT -p tcp -s 1.2.3.4 --dport 22 -j ACCEPT    # allow ssh from 1.2.3.4
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp --dport 110 -j ACCEPT
iptables -A INPUT -p tcp --dport 995 -j ACCEPT
iptables -A INPUT -j DROP   # block the rest of the traffic

```
Remember for outbound connections dport is sport for source port and -s for source is -d for destination.



### ssh

Use these two to only allow ssh to your computer from the example ip of 192.168.174.129.
```bash
iptables -I INPUT -p tcp -s 192.168.174.129 --dport 22 -j ACCEPT
iptables -I OUTPUT -p tcp -d 192.168.174.129 --sport 22 -j ACCEPT

```



## Saving iptables Rules

#### Saving Rules
The current rules you have are only saved in memory and will not persist through a reboot. to save the rules so that iptables reconfigured is `/sbin/iptables-save` or just `iptables-save`. If you with to restore the previously saved rules, use `iptables-restore`
#### Saving rules to a file
```bash
iptables-save > ~/iptables.rules
```
Restoring rules from a file.
```bash
iptables-restore < ~/iptables.rules
```




## Extra information
### Logging

In the above examples none of the traffic will be logged. If you would like to log dropped packets to syslog, this would be the quickest way:

```bash
sudo iptables -I INPUT 5 -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7
```

